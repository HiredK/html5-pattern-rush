var vendors = ['webkit', 'moz'];
for (var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
    window.requestAnimationFrame = window[vendors[x] + 'RequestAnimationFrame'];
    window.cancelAnimationFrame = window[vendors[x] + 'CancelAnimationFrame'] ||
    window[vendors[x] + 'CancelRequestAnimationFrame'];
}

var html5_audiotypes = {
    "mp3": "audio/mpeg",
    "mp4": "audio/mp4",
    "ogg": "audio/ogg",
    "wav": "audio/wav"
}

// create fps stats
var stats = new Stats();

//document.body.appendChild(stats.domElement);
//stats.setMode(0); // 0: fps, 1: ms

// align top-left / front
//stats.domElement.style.position = 'absolute';
//stats.domElement.style.zIndex = "2";
//stats.domElement.style.left = '18px';
//stats.domElement.style.top = '18px';

(function () {
    // define variables
    var canvas = document.getElementById('canvas');
    var cx = canvas.getContext('2d');
    var cw = canvas.width;
    var ch = canvas.height;
    var stop;

    // timing variables
    var fps = 60;
    var interval = 1000 / fps;
    var lastTime = (new Date()).getTime();
    var currentTime = 0;
    var delta = 0;

    var GameState =
    {
        STATE_0: { value: 0, name: "InMenu" },
        STATE_1: { value: 1, name: "InGame_ShowCard" },
        STATE_2: { value: 2, name: "InGame_Finished" },
        STATE_3: { value: 3, name: "InGame_Finished_Special" },
    };

    // current game state
    var currentTimeScale = 1;
    var currentLevel = 0;
    var currentState;
    var score = 0;

    var closeToObstacle = false;
    var levelLength = 0;
    var obstaclesFrequency = 0;

    var userMadeChoice = false;
    var userWonLevel = false;
    var userWonTimer = 0;
    var tutorialTimer = 0;
    var specialTimer = 0;

    var numObstaclesDone = 0;
    var totalNumObstaclesDone = 0;
    var obstaclesResult = [];

    // game elements
    var ground = [], obstacles = []

    // music
    var music;
    var moont;

    /**
     * Returns a random integer between min (inclusive) and max (inclusive)
     * Using Math.round() will give you a non-uniform distribution!
     */
    function getRandomInt(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    /*/////////////////////////////////////////////////
    /// ASSET LOADER
    /////////////////////////////////////////////////*/
    /**
     * Asset pre-loader object. Loads all images
     */
    var assetLoader = (function () {
        // images dictionary
        this.imgs =
        {
            'bg': 'data/bg.png',
            'sky': 'data/sky.png',
            'overlay': 'data/overlay.png',
            'backdrop1': 'data/backdrop1.png',
            'backdrop2': 'data/backdrop2.png',
            'backdrop3': 'data/backdrop3.png',
            'backdrop4': 'data/backdrop4.png',
            'hud_left': 'data/hud-loc-left.png',
            'hud_middle': 'data/hud-loc-middle.png',
            'hud_right': 'data/hud-loc-right.png',
            'normal_walk': 'data/normal_walk.png',
            'grass': 'data/grass.png',
            'obstacle0': 'data/obstacle0.png',
            'obstacle1': 'data/obstacle1.png',
            'obstacle2': 'data/obstacle2.png',
            'obstacle3': 'data/obstacle3.png',
            'hole': 'data/hole.png',
            'flip': 'data/flip.png',
            'action': 'data/action.png',
            'title': 'data/title.png',
            'tutorial': 'data/tutorial.png',
            'card_0': 'data/card_0.png',
            'card_1': 'data/card_1.png',
            'card_2': 'data/card_2.png',
            'card_3': 'data/card_3.png',
            'card_4': 'data/card_4.png',
            'card_5': 'data/card_5.png',
            'card': 'data/card.png',
        };

        // how many assets have been loaded
        var assetsLoaded = 0;

        // total number of image assets
        var numImgs = Object.keys(this.imgs).length;

        // total number of assets
        this.totalAssest = numImgs;

        /**
         * Ensure all assets are loaded before using them
         * @param {number} dic  - Dictionary name ('imgs', 'sounds', 'fonts')
         * @param {number} name - Asset name in the dictionary
         */
        function assetLoaded(dic, name) {
            // don't count assets that have already loaded
            if (this[dic][name].status !== 'loading') {
                return;
            }

            this[dic][name].status = 'loaded';
            assetsLoaded++;

            // finished callback
            if (assetsLoaded === this.totalAssest && typeof this.finished === 'function') {
                this.finished();
            }
        }

        /**
         * Create assets, set callback for asset loading, set asset source
         */
        this.downloadAll = function () {
            var _this = this;
            var src;

            // load images
            for (var img in this.imgs) {
                if (this.imgs.hasOwnProperty(img)) {
                    src = this.imgs[img];

                    // create a closure for event binding
                    (function (_this, img) {
                        _this.imgs[img] = new Image();
                        _this.imgs[img].status = 'loading';
                        _this.imgs[img].name = img;
                        _this.imgs[img].onload = function () { assetLoaded.call(_this, 'imgs', img) };
                        _this.imgs[img].src = src;
                    })(_this, img);
                }
            }
        }

        return {
            imgs: this.imgs,
            totalAssest: this.totalAssest,
            downloadAll: this.downloadAll
        };
    })();

    assetLoader.finished = function ()
    {
        // create loopable music
        music = new Audio("data/Music.mp3");
        music.addEventListener('ended', function () {
            this.currentTime = 0;
            this.play();
        }, false);

        currentState = GameState.STATE_0;
        tutorialTimer = 200;
        music.play();
        gameLoop();
    }

    /*/////////////////////////////////////////////////
    /// SOUND SYSTEM
    /////////////////////////////////////////////////*/
    /**
     * Instantiate script by calling: var uniquevar=createsoundbite("soundfile1", "fallbackfile2", "fallebacksound3", etc)
     * Call: uniquevar.playclip() to play sound
     */
    function createsoundbite(sound) {
        var html5audio = document.createElement('audio')
        if (html5audio.canPlayType)
        {
            // check support for HTML5 audio
            for (var i = 0; i < arguments.length; i++) {
                var sourceel = document.createElement('source')
                sourceel.setAttribute('src', arguments[i])
                if (arguments[i].match(/\.(\w+)$/i))
                    sourceel.setAttribute('type', html5_audiotypes[RegExp.$1])
                html5audio.appendChild(sourceel)
            }

            html5audio.load()
            html5audio.playclip = function ()
            {
                html5audio.pause()
                html5audio.currentTime = 0
                html5audio.play()
            }

            return html5audio;
        }
        else {
            return { playclip: function () { throw new Error("Your browser doesn't support HTML5 audio unfortunately") } }
        }
    }

    var levelSound = createsoundbite("data/Big_Success_01.wav");
    var clickSound = createsoundbite("data/Click_Default_01.wav");
    var goodSound = createsoundbite("data/Success_01.wav");
    var failSound = createsoundbite("data/Fail_01.wav");

    /*/////////////////////////////////////////////////
    /// SPRITE SHEET
    /////////////////////////////////////////////////*/
    /**
     * Create a sprite sheet
     */
    function SpriteSheet(path, frameW, frameH)
    {
        this.image = new Image();
        this.frameW = frameW;
        this.frameH = frameH;

        // calculate the number of frames in a row after the image loads
        var self = this;
        this.image.onload = function () {
            self.framesPerRow = Math.floor(self.image.width / self.frameW);
        };

        this.image.src = path;
    }

    /*/////////////////////////////////////////////////
    /// ANIMATION
    /////////////////////////////////////////////////*/
    /**
     * Create an animation from a spritesheet
     */
    function Animation(spritesheet, frameSpeed, startFrame, endFrame)
    {
        var animationSequence = [];
        var currentFrame = 0;
        var counter = 0;

        this.frameSpeed = frameSpeed;

        // start and end range for frames
        for (var frameNumber = startFrame; frameNumber <= endFrame; frameNumber++) {
            animationSequence.push(frameNumber);
        }

        /**
         * Update the animation
         */
        this.update = function () {
            // update to the next frame if it is time
            if (counter == (frameSpeed - 1))
                currentFrame = (currentFrame + 1) % animationSequence.length;

            // update the counter
            counter = (counter + 1) % this.frameSpeed;
        };

        /**
         * Draw the current frame
         */
        this.draw = function (x, y) {
            // get the row and col of the frame
            var row = Math.floor(animationSequence[currentFrame] / spritesheet.framesPerRow);
            var col = Math.floor(animationSequence[currentFrame] % spritesheet.framesPerRow);

            cx.drawImage(
              spritesheet.image,
              col * spritesheet.frameW, row * spritesheet.frameH,
              spritesheet.frameW, spritesheet.frameH,
              x, y,
              spritesheet.frameW, spritesheet.frameH);
        };
    }

    /*/////////////////////////////////////////////////
    /// ACTION ANIM
    /////////////////////////////////////////////////*/
    /**
     * The action animation object
     */
    function ActionAnim() {
        this.sheet = new SpriteSheet('data/action.png', cw, ch);
        this.actionAnim = new Animation(this.sheet, 25, 0, 1);
        this.anim = this.actionAnim;

        /**
         * Update the action
         */
        this.update = function () {
            this.anim.update();
        }

        /**
         * Draw the action
         */
        this.draw = function ()
        {
            this.anim.draw(0, 0);
        }
    }

    /*/////////////////////////////////////////////////
    /// PARALLAX BACKGROUND
    /////////////////////////////////////////////////*/
    /**
     * Create a parallax background
     */
    var background = (function ()
    {
        var sky = {};
        var backdrop1 = {};
        var backdrop2 = {};
        var backdrop3 = {};
        var backdrop4 = {};

        var anim = new ActionAnim();

        /**
         * Update the animation
         */
        this.update = function ()
        {
            if (closeToObstacle && !userMadeChoice) {
                anim.update();
            }
            else {
                // pan background
                sky.x -= sky.speed * currentTimeScale;
                backdrop1.x -= backdrop1.speed * currentTimeScale;
                backdrop2.x -= backdrop2.speed * currentTimeScale;
                backdrop3.x -= backdrop3.speed * currentTimeScale;
                if (userWonLevel)
                {
                    backdrop4.x -= backdrop4.speed;
                }

                // If the image scrolled off the screen, reset
                if (sky.x + assetLoader.imgs.sky.width <= 0) {
                    sky.x = 0;
                }
                if (backdrop2.x + assetLoader.imgs.backdrop1.width <= 0) {
                    backdrop2.x = 0;
                }
                if (backdrop2.x + assetLoader.imgs.backdrop2.width <= 0) {
                    backdrop2.x = 0;
                }
                if (backdrop3.x + assetLoader.imgs.backdrop3.width <= 0) {
                    backdrop3.x = 0;
                }
            }
        }

        /**
         * Draw the backgrounds to the screen at different speeds
         */
        this.draw = function ()
        {
            if (closeToObstacle && !userMadeChoice) {
                anim.draw();
            }
            else {
                cx.drawImage(assetLoader.imgs.bg, 0, 0);

                // draw images side by side to loop
                cx.drawImage(assetLoader.imgs.sky, sky.x, sky.y);
                cx.drawImage(assetLoader.imgs.sky, sky.x + canvas.width, sky.y);

                cx.drawImage(assetLoader.imgs.backdrop1, backdrop1.x, backdrop1.y);
                cx.drawImage(assetLoader.imgs.backdrop1, backdrop1.x + canvas.width, backdrop1.y);

                cx.drawImage(assetLoader.imgs.backdrop2, backdrop2.x, backdrop2.y);
                cx.drawImage(assetLoader.imgs.backdrop2, backdrop2.x + canvas.width, backdrop2.y);

                cx.drawImage(assetLoader.imgs.backdrop3, backdrop3.x, backdrop3.y);
                cx.drawImage(assetLoader.imgs.backdrop3, backdrop3.x + canvas.width, backdrop3.y);

                if (userWonLevel) {
                    cx.drawImage(assetLoader.imgs.backdrop4, backdrop4.x, backdrop4.y);
                }
                else {
                    backdrop4.x = 400;
                }
            }
        }

        /**
         * Reset background to zero
         */
        this.reset = function () {
            sky.x = 0;
            sky.y = 0;
            sky.speed = 0.2;

            backdrop1.x = 0;
            backdrop1.y = 0;
            backdrop1.speed = 0.2;

            backdrop2.x = 0;
            backdrop2.y = 0;
            backdrop2.speed = 0.4;

            backdrop3.x = 0;
            backdrop3.y = 0;
            backdrop3.speed = 0.6;

            backdrop4.x = 0;
            backdrop4.y = 0;
            backdrop4.speed = 1.5;
        }

        return {
            update: this.update,
            draw: this.draw,
            reset: this.reset
        };
    })();

    /*/////////////////////////////////////////////////
    /// VECTOR2D
    /////////////////////////////////////////////////*/
    /**
     * A vector for 2d space
     */
    function Vector(x, y, dx, dy)
    {
        // position
        this.x = x || 0;
        this.y = y || 0;

        // direction
        this.dx = dx || 0;
        this.dy = dy || 0;
    }

    /**
     * Advance the vectors position by dx,dy
     */
    Vector.prototype.advance = function () {
        this.x += this.dx;
        this.y += this.dy;
    };

    /**
     * Get the minimum distance between two vectors
     * @param {Vector}
     * @return minDist
     */
    Vector.prototype.minDist = function (vec)
    {
        var minDist = Infinity;
        var max = Math.max(Math.abs(this.dx), Math.abs(this.dy),
                                Math.abs(vec.dx), Math.abs(vec.dy));
        var slice = 1 / max;

        var x, y, distSquared;

        // get the middle of each vector
        var vec1 = {}, vec2 = {};
        vec1.x = this.x + this.width / 2;
        vec1.y = this.y + this.height / 2;
        vec2.x = vec.x + vec.width / 2;
        vec2.y = vec.y + vec.height / 2;
        for (var percent = 0; percent < 1; percent += slice) {
            x = (vec1.x + this.dx * percent) - (vec2.x + vec.dx * percent);
            y = (vec1.y + this.dy * percent) - (vec2.y + vec.dy * percent);
            distSquared = x * x + y * y;

            minDist = Math.min(minDist, distSquared);
        }

        return Math.sqrt(minDist);
    };

    /*/////////////////////////////////////////////////
    /// PLAYER OBJECT
    /////////////////////////////////////////////////*/
    /**
     * The player object
     */
    var player = (function(player)
    {
        // add properties directly to the player imported object
        player.width     = 64;
        player.height    = 64;
        player.speed     = 3;

        // spritesheets
        player.sheet     = new SpriteSheet('data/normal_walk.png', player.width, player.height);
        player.walkAnim = new Animation(player.sheet, 4, 0, 7);
        player.jumpAnim = new Animation(player.sheet, 15, 8, 11);
        player.failAnim = new Animation(player.sheet, 10, 11, 18);
        player.anim = player.walkAnim;

        player.jumpTimer = 0;
        player.jumping = false;

        player.failTimer = 0;
        player.failing = false;

        Vector.call(player, 0, 0, 0, 0);

        /**
         * Update the player's position and animation
         */
        player.update = function()
        {
            if (closeToObstacle) {
                player.walkAnim.frameSpeed = 15;
            }
            else {
                player.walkAnim.frameSpeed = 4;
            }

            if (player.jumpTimer > 0) {
                if (player.y > 172 - 28) {
                    player.y -= 2;
                }

                player.jumpTimer--;
            }
            else
            if (player.failTimer > 0) {

                player.failTimer--;
            }
            else {
                if (player.y <= 172) {
                    player.y += 2;
                }

                player.anim = player.walkAnim;

                player.jumpTimer = 0;
                player.jumping = false;

                player.failTimer = 0;
                player.failing = false;
            }

            this.advance();
            player.anim.update();
        };

        /**
         * Draw the player at it's current position
         */
        player.draw = function()
        {
            player.anim.draw(player.x, player.y);
        };

        /**
         * Makes the player perform a jump animation
         */
        player.jump = function()
        {
            player.anim = player.jumpAnim;
            player.anim.currentFrame = 0;
            player.jumpTimer = 40;
            player.jumping = true;
        }

        /**
         * Makes the player perform a fail animation
         */
        player.fail = function ()
        {
            player.anim = player.failAnim;
            player.anim.currentFrame = 0;
            player.failTimer = 60;
            player.failing = true;
        }

        /**
         * Reset the player's position
         */
        player.reset = function()
        {
            player.x = 250;
            player.y = 172;
        };

        return player;
    })(Object.create(Vector.prototype));

    /*/////////////////////////////////////////////////
    /// CARD OBJECT
    /////////////////////////////////////////////////*/
    /**
     * The card object
     */
    function Card(img)
    {
        this.front = img;
        this.back = assetLoader.imgs.card;

        this.sheet = new SpriteSheet('data/flip.png', 120, 150);
        this.flipAnim = new Animation(this.sheet, 4, 0, 3);
        this.anim = this.flipAnim;

        this.flipping = false;
        this.flipTimer = 0;

        /**
         * Update the card
         */
        this.update = function()
        {
            if (this.flipTimer > 0)
            {
                this.flipTimer--;
            }
            else
            {
                this.flipTimer = 0;
                this.flipping = false;
            }

            this.anim.update();
        }

        /**
         * Flips the card
         */
        this.flip = function()
        {
            this.flipTimer = 75;
            this.flipping = true;
        }

        /**
         * Draw the card
         */
        this.drawAt = function (flipped, x, y)
        {
            if (this.flipping) {
                if (this.flipTimer < 50) {
                    cx.drawImage(this.front, x, y);
                }
                else {
                    this.anim.draw(x, y);
                }
            }
            else {
                if (!flipped) {
                    cx.drawImage(this.front, x, y);
                }
                else {
                    cx.drawImage(this.back, x, y);
                }
            }
        }
    }

    /*/////////////////////////////////////////////////
    /// SPRITE OBJECT
    /////////////////////////////////////////////////*/
    /**
     * Create a sprite object
     */
    function Sprite(x, y, type)
    {
        this.x = x;
        this.y = y;
        this.type = type;

        Vector.call(this, x, y, 0, 0);

        /**
         * Update the Sprite's position by the player's speed
         */
        this.update = function ()
        {
            this.dx = -player.speed * currentTimeScale;
            this.advance();
        };

        /**
         * Draw the sprite at it's current position
         */
        this.draw = function ()
        {
            cx.save();
            cx.translate(0.5, 0.5);
            cx.drawImage(assetLoader.imgs[this.type], this.x, this.y);
            cx.restore();
        };
    }
    Sprite.prototype = Object.create(Vector.prototype);

    /*/////////////////////////////////////////////////
    /// HUD DISPLAY
    /////////////////////////////////////////////////*/
    /**
     * Create a HUD Display
     */
    var hud = (function ()
    {
        /**
         * Draw the board
         */
        this.draw = function (offset_x, offset_y)
        {
            // draw left hud loc
            cx.drawImage(assetLoader.imgs.hud_left, offset_x, offset_y);

            // draw middle hud loc
            num_obstacles = Math.ceil(levelLength / obstaclesFrequency);
            for (i = 1; i < num_obstacles-2; i++) {
                cx.drawImage(assetLoader.imgs.hud_middle, offset_x + (i * 24), offset_y);
            }

            // draw right hud loc
            cx.drawImage(assetLoader.imgs.hud_right, offset_x + ((num_obstacles-2) * 24), offset_y);

            // draw result display
            for (i = 0; i < numObstaclesDone; i++)
            {
                cx.beginPath();
                cx.rect(offset_x + (i * 24) + 8, offset_y + 8, 8, 8);
                cx.fillStyle = (obstaclesResult[i] == 1) ? 'green' : 'red';
                cx.fill();
            }
        }

        return {
            draw: this.draw,
        };
    })();

    /*/////////////////////////////////////////////////
    /// BOARD/CARDS
    /////////////////////////////////////////////////*/
    /**
     * Create a board object
     */
    var board = (function ()
    {
        var currentHoveredCard = -1;
        var offset = 34;

        var hiddenCard;
        var cardsList = [];
        var cards = [];

        /**
         * Draw the board
         */
        this.isHovering = function (mx, my)
        {
            // hard coded
            x = (offset * 1) + (120 * 0);
            if (mx > x && mx < x + 120 && my > 300 && my < 450) {
                return 0;
            }

            x = (offset * 2) + (120 * 1);
            if (mx > x && mx < x + 120 && my > 300 && my < 450) {
                return 1;
            }

            x = (offset * 3) + (120 * 2);
            if (mx > x && mx < x + 120 && my > 300 && my < 450) {
                return 2;
            }

            x = (offset * 4) + (120 * 3);
            if (mx > x && mx < x + 120 && my > 300 && my < 450) {
                return 3;
            }

            x = (offset * 5) + (120 * 4);
            if (mx > x && mx < x + 120 && my > 300 && my < 450) {
                return 4;
            }

            return -1;
        }

        /**
         * Update the board
         */
        this.update = function ()
        {
            for (i = 0; i < 5; i++) {
                cards[i].update();
            }

            hiddenCard.update();
        }

        /**
         * Update the board
         */
        this.isFlipping = function ()
        {
            for (i = 0; i < 5; i++) {
                if (cards[i].flipping) {
                    return true;
                }
            }

            return false;
        }

        /**
         * Draw the board
         */
        this.draw = function ()
        {
            // hard coded
            x = (offset * 1) + (120 * 0);
            cards[0].drawAt(closeToObstacle, x, 300);

            x = (offset * 2) + (120 * 1);
            cards[1].drawAt(closeToObstacle, x, 300);

            x = (offset * 3) + (120 * 2);
            cards[2].drawAt(closeToObstacle, x, 300);

            x = (offset * 4) + (120 * 3);
            cards[3].drawAt(closeToObstacle, x, 300);

            x = (offset * 5) + (120 * 4);
            cards[4].drawAt(closeToObstacle, x, 300);

            // obstacle card (?..)
            hiddenCard.drawAt(!closeToObstacle, 575, 55);
        }

        /**
         * Init CardList
         */
        this.initCardList = function ()
        {            
            cardsList[0] = new Card(assetLoader.imgs.card_0);
            cardsList[0].card_id = 0;
            cardsList[1] = new Card(assetLoader.imgs.card_1);
            cardsList[1].card_id = 1;
            cardsList[2] = new Card(assetLoader.imgs.card_2);
            cardsList[2].card_id = 2;
            cardsList[3] = new Card(assetLoader.imgs.card_3);
            cardsList[3].card_id = 3;
            cardsList[4] = new Card(assetLoader.imgs.card_4);
            cardsList[4].card_id = 4;
            cardsList[5] = new Card(assetLoader.imgs.card_5);
            cardsList[5].card_id = 5;
        }


        /**
         * Randomize the cards
         */
        this.rand = function ()
        {
            // randomize cardlist
            for(i = 0; i < 20; i++)
            {
                var rand1 = getRandomInt(0, 5);
                var rand2 = getRandomInt(0, 5);
                var temp = cardsList[rand1];
                cardsList[rand1] = cardsList[rand2];
                cardsList[rand2] = temp;
            }

            // put card in cards from the randomized cardslist 
            // with matching index
            for (i = 0; i < 5; i++)
            {
                cards[i] = cardsList[i];
            }
            
            // set the hidden card
            rand = getRandomInt(0, 4);
            hiddenCard = new Card(cards[rand].front);
            hiddenCard.card_id = cards[rand].card_id;
        }

        /**
         * mousemove callback
         */
        this.onMove = function (event)
        {
            if (currentState == GameState.STATE_0 ||
                currentState == GameState.STATE_2) {
                event.target.style.cursor = 'pointer';
                return;
            }

            if (closeToObstacle) {
                if ((currentHoveredCard = isHovering(event.pageX, event.pageY)) >= 0) {
                    event.target.style.cursor = 'pointer';
                    return;
                }
            }

            event.target.style.cursor = 'default';
        }

        /**
         * mousedown callback
         */
        this.onDown = function (event)
        {
            if (currentState == GameState.STATE_0 ||
                currentState == GameState.STATE_2)
            {
                // play click sound
                clickSound.playclip();

                // reset music
                music.currentTime = 0;

                // reset user score
                totalNumObstaclesDone = 0;
                currentLevel = 0;
                score = 0;

                startGame(currentLevel++);
                return;
            }

            if (!userMadeChoice) {
                if (closeToObstacle && currentHoveredCard >= 0) {
                    if (hiddenCard.card_id == cards[currentHoveredCard].card_id) {
                        obstaclesResult[numObstaclesDone] = 1;
                        goodSound.playclip();
                        player.jump();
                        score++;
                    }
                    else {
                        obstaclesResult[numObstaclesDone] = 0;
                        failSound.playclip();
                        player.fail();
                    }

                    cards[currentHoveredCard].flip();
                    userMadeChoice = true;
                }
            }
        }

        return {
            update: this.update,
            draw: this.draw,
            rand: this.rand,
            isFlipping: this.isFlipping,
            initCardList: this.initCardList,

            onMove: this.onMove,
            onDown: this.onDown
        };
    })();

    /*/////////////////////////////////////////////////
    /// GAME LOOP
    /////////////////////////////////////////////////*/
    /**
     * Start the game 
     */
    function startGame(level_num)
    {
        switch (level_num) {
            case 0: // level0
                startLevel(150, 35);
                break;

            case 1: // level1
                startLevel(200, 30);
                break;

            case 2: // level2
                startLevel(200, 25);
                break;

            case 3: // level3
                startLevel(250, 20);
                break;

            case 4: // level4
                startLevel(300, 20);
                break;

            case 5: // level5
                startLevel(400, 15);
                break;

            default:
                currentState = GameState.STATE_2;
                break;
        }

        // start game-loop
        gameLoop();
    }

    /**
     * Start the level
     */
    function startLevel(level_length, obstacles_frequency)
    {
        obstacles = []
        ground = []

        levelLength = level_length;
        obstaclesFrequency = obstacles_frequency;
        numObstaclesDone = 0;
        userWonLevel = false;
        userWonTimer = 0;

        num_obstacles = Math.ceil(levelLength / obstaclesFrequency);
        for (i = 0; i < num_obstacles; i++) {
            obstaclesResult[i] = 0;
        }

        // reset level value
        background.reset();
        player.reset();

        // reset board values
        board.initCardList();
        board.rand();

        // spawn level ground
        for (var i = 0; i < level_length + 48; i++)
        {
            if (i > level_length) {
                ground.push(new Sprite(i * 32, 232, 'grass'));
                continue;
            }

            is_hole = false;
            if ((i % obstacles_frequency) == 0) {
                if (Math.random() > 0.25)
                {
                    type = 'obstacle0';
                    switch (getRandomInt(0, 3)) {
                        case 0:
                            type = 'obstacle0';
                            break;
                        case 1:
                            type = 'obstacle1';
                            break;
                        case 2:
                            type = 'obstacle2';
                            break;
                        case 3:
                            type = 'obstacle3';
                            break;
                        default:
                            type = 'obstacle0';
                            break;
                    }
                    obstacles.push(new Sprite(i * 32, 200, type));
                }
                else
                    is_hole = true;
            }

            if (is_hole) {
                obstacles.push(new Sprite(i * 32, 232, 'hole'));
            }
            else {
                ground.push(new Sprite(i * 32, 232, 'grass'));
            }
        }

        currentState = GameState.STATE_1;
        stop = false;
    }

    /**
     * Display the current score
     */
    function showScore(style)
    {
        cx.fillStyle = "black";
        cx.font = "16px Courier";
        cx.fillText("Level: " + currentLevel, canvas.width - 116, 32);
        cx.fillText("Score: " + score, canvas.width - 116, 48);
    }

    /**
     * Game loop
     */
    function gameLoop() {
        if (!stop) {
            window.requestAnimationFrame(gameLoop);
            currentTime = (new Date()).getTime();
            delta = (currentTime - lastTime);

            if (delta > interval)
            {
                //stats.begin();
                cx.clearRect(0, 0, canvas.width, canvas.height);
                switch (currentState.value)
                {
                    // InMenu
                    case GameState.STATE_0.value:
                        cx.drawImage(assetLoader.imgs.title, 0, 0);
                        currentTimeScale = 0.0;
                        music.volume = 0.0;

                        cx.fillStyle = "white";
                        cx.font = "32px Arial";
                        cx.fillText("Click To Start!", (cw / 2) - 100, (ch / 2) + 16);
                        cx.fillStyle = "black";
                        break;

                    // InGame_ShowCard
                    case GameState.STATE_1.value:
                    {
                        music.volume = 0.25;
                        background.update();
                        background.draw();

                        for (var i = 0; i < ground.length; i++) {
                            ground[i].update();
                            ground[i].draw();
                        }

                        closest_obstacle = 10000;
                        for (var i = 0; i < obstacles.length; i++)
                        {
                            dist = obstacles[i].x - player.x;
                            if (dist >= 0 && closest_obstacle > dist) {
                                closest_obstacle = dist;
                            }

                            obstacles[i].update();
                            obstacles[i].draw();
                        }

                        cx.drawImage(assetLoader.imgs.overlay, 0, 0);
                        hud.draw(28, 28);

                        player.update();
                        player.draw();

                        if (numObstaclesDone == obstacles.length - 1 && !board.isFlipping()) {
                            if (!userWonLevel) {
                                userWonLevel = true;
                                userWonTimer = 250;
                            }

                            if (userWonTimer == 200) {
                                levelSound.playclip();
                            }
                            else {
                                if (userWonTimer <= 0) {
                                    totalNumObstaclesDone += numObstaclesDone;
                                    startGame(currentLevel++);
                                }
                            }

                            userWonTimer--;
                        }
                        else {
                            board.update();
                            board.draw();
                        }

                        if (!player.jumping && !player.failing && closest_obstacle < 125 && closest_obstacle > 50) {
                            closeToObstacle = true;
                            currentTimeScale = 0.2;
                        }
                        else {
                            currentTimeScale = 1.0;
                            if (closeToObstacle && !board.isFlipping())
                            {
                                // still a fail if we did nothing
                                if (!userMadeChoice) {
                                    failSound.playclip();
                                    player.fail();
                                }

                                // reset board values
                                board.initCardList();
                                board.rand();

                                closeToObstacle = false;
                                userMadeChoice = false;
                                numObstaclesDone++;
                            }
                        }

                        if (tutorialTimer > 0) {
                            cx.drawImage(assetLoader.imgs.tutorial, 0, 0);
                            currentTimeScale = 0.5;
                            tutorialTimer--;
                        }
                        else {
                            tutorialTimer = 0;
                        }

                        showScore();
                    } break;

                    // InGame_Finished
                    case GameState.STATE_2.value:
                    {
                        if (score == totalNumObstaclesDone) {
                            currentState = GameState.STATE_3;
                            specialTimer = 1000;

                            music.volume = 0;
                            moont = new Audio("data/Moon.mp3");
                            moont.play();
                            break;
                        }

                        cx.drawImage(assetLoader.imgs.title, 0, 0);
                        currentTimeScale = 0.0;
                        music.volume = 0.05;

                        cx.fillStyle = "white";
                        cx.font = "32px Arial";
                        cx.fillText("Total Score: " + score + " / " + totalNumObstaclesDone, (cw / 2) - 132, (ch / 2) + 16);
                        cx.fillStyle = "black";
                    } break;

                    // InGame_Finished_Special
                    case GameState.STATE_3.value:
                    {
                        music.volume = 0.0;
                        if (specialTimer > 0) {
                            cx.fillStyle = "black";
                            cx.fillRect(0, 0, cw, ch);

                            cx.fillStyle = "white";
                            cx.font = "italic  32px Arial";
                            cx.fillText("SEE YOU SPACE COWBOY...", cw - 500, ch - 32);
                            cx.fillStyle = "black";
                            specialTimer--;
                        }
                        else {
                            currentState = GameState.STATE_0;
                            specialTimer = 0;
                        }
                    } break;

                    default:
                        break;
                }

                lastTime = currentTime - (delta % interval);
                //stats.end();
            }
        }
    }

    /**
     * Called when the mouse is moving
     */
    canvas.addEventListener("mousemove", onMouseMove, false);
    function onMouseMove(event) {
        board.onMove(event);
    }

    /**
     * Called when the mouse is clicked
     */
    canvas.addEventListener("mousedown", onMouseDown, false);
    function onMouseDown(event) {
        board.onDown(event);
    }

    assetLoader.downloadAll();
})();